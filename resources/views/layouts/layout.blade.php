<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/font-awesome-4.7.0/css/font-awesome.min.css') }}">
    @yield('css')
</head>
<body>
    @include('layouts.frontend.header')
    @include('layouts.frontend.menu')
    <div class="container-fluid">
        @yield('content')
    </div>
    @include('layouts.frontend.footer')
    <script src="{{ asset('js/app.js') }}"></script>
    @yield('js')
</body>
</html>
