<div class="conatiner-fluid">
    <div id="header-top">
        <div class="row bg-dark">
            <div class="col-11 mx-auto">
                <div class="row mt-3">
                    <div class="col-4 text-left align-items-center">
                        <p class="text-white nb-social-header">Liên kết với chúng tôi:
                            <a href="" class="nb-icon-bg ml-2 mr-2"><i class="fa fa-facebook bg-primary fa-lg" aria-hidden="true"></i></a>
                            <a href="" class="nb-icon-bg mr-2"><i class="fa fa-twitter bg-info fa-lg" aria-hidden="true"></i></a>
                            <a href="" class="nb-icon-bg mr-2"><i class="fa fa-youtube bg-danger fa-lg" aria-hidden="true"></i></a>
                        </p>
                    </div>
                    <div class="col-4 text-center align-items-center">
                        <p class="text-white "><i class="fa fa-address-book fa-lg" aria-hidden="true"></i> Email: example.gmail.com</p>
                    </div>
                    <div class="col-4 text-right align-items-center">
                        <ul>
                            <p  class="text-white"><i class="fa fa-map-marker fa-lg" aria-hidden="true"></i> Hệ Thống Showroom: 586 Lê Văn Khương, P. Thới An, Quận 12

                            </p>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="header-content">

    </div>
</div>

